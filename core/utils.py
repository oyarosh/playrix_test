#!/usr/bin/python3
import csv
import sys
import datetime
from decimal import Decimal

from collections import OrderedDict

patter1 = "%Y-%m-%d %H:%M:%S"
patter2 = "%d.%m.%Y %H:%M:%S"


class ProcessCSVMixin(object):
    """
        CSV reader. Organizes reading/formatting of CSV file
    """
    def __init__(self, filepath, delimiter=',', *args, **kwargs):
        """
            :param filepath: absolute path to the file
            :type filepath: str
            :param delimiter: file delimiter. Default: ','
            :type delimiter: str
        """
        self.filepath = filepath
        self.delimiter = delimiter
        self.fields = ['created', 'mobile_app', 'country']
        self.start = datetime.datetime.strptime("02.05.2016 00:00:00", patter2)
        self.end = datetime.datetime.strptime("09.05.2016 23:59:59", patter2)

    def is_int(self, elem):
        try:
            return int(elem)
        except ValueError:
            return False

    def row_map(self, row):
        return dict(zip(self.fields, row))

    def process_row_from_csv(self):
        with open(self.filepath, 'rt') as csvfile:
            reader = csv.reader(csvfile, delimiter=self.delimiter)
            for x, row in enumerate(reader):
                # Skipping empty rows
                if len(row) == 1:
                    continue
                result = self.row_map(row)
                if result['mobile_app'] != '2':
                    continue
                yield result


class InstallsProcessCSV(ProcessCSVMixin):
    def __init__(self, *args, **kwargs):
        super(InstallsProcessCSV, self).__init__(*args, **kwargs)
        self.fields = ['created', 'mobile_app', 'country']

    def process_row_from_csv(self):
        for result in super(InstallsProcessCSV, self).process_row_from_csv():
            created = datetime.datetime.strptime(result['created'], patter1)
            if created < self.start or created > self.end:
                continue
            yield result


class PurchasesProcessCSV(ProcessCSVMixin):
    def __init__(self, *args, **kwargs):
        super(PurchasesProcessCSV, self).__init__(*args, **kwargs)
        self.fields = ['created', 'mobile_app', 'country', 'install_date', 'revenue']

    def process_row_from_csv(self):
        for result in super(PurchasesProcessCSV, self).process_row_from_csv():
            install_date = datetime.datetime.strptime(result['install_date'], patter1)
            if install_date < self.start or install_date > self.end:
                continue
            yield result


class GetResult:
    def __init__(self, installs_path, purchases_path, *args, **kwargs):
        self.installs_path = installs_path
        self.purchases_path = purchases_path

    def process(self):
        installs_map = {}
        for row in InstallsProcessCSV(self.installs_path).process_row_from_csv():
            key = (row.get('country'), row.get('mobile_app'))
            installs_map[key] = installs_map.get(key, 0) + 1
        order_installs_map = OrderedDict(
            sorted(installs_map.items(), key=lambda x: x[1], reverse=True))

        sys.stdout.write('installs.csv finished processing\r\n')

        purchases_map = {}
        for row in PurchasesProcessCSV(self.purchases_path).process_row_from_csv():
            key = (row.get('country'), row.get('mobile_app'))
            install_date = datetime.datetime.strptime(row['install_date'], patter1)
            created = datetime.datetime.strptime(row['created'], patter1)
            date_diff = created - install_date
            days = date_diff.total_seconds() / (24 * 60 * 60)
            if days > 10:
                continue

            num = int(days) if days == int(days) else int(days) + 1
            purchases_map.setdefault(key, {})['sum_revenue' + str(num)] = \
                purchases_map.get(key, {}).get('sum_revenue' + str(num), Decimal(0)) + Decimal(row.get('revenue', 0))

        sys.stdout.write('purchases.csv finished processing\r\n')

        result_map = []
        for key, value in order_installs_map.items():
            rpi_map = OrderedDict([('country', key[0]), ('installs', value)])
            for rpi_num in range(1, 11):
                rpi_map['rpi' + str(rpi_num)] = \
                    '{:.2f}'.format(purchases_map[key]['sum_revenue' + str(rpi_num)] / value)
            result_map.append(rpi_map)

        return result_map
