#!/usr/bin/python3
import unittest
import unittest.mock as mock

from core.utils import GetResult

# Just in case that I could do it)))


class GetResultTest(unittest.TestCase):

    @mock.patch('core.utils.InstallsProcessCSV.process_row_from_csv')
    @mock.patch('core.utils.PurchasesProcessCSV.process_row_from_csv')
    def test_get_result_process(self, mock_installs_file, mock_purchases_file):
        mock_installs_file.side_effect = OSError
        with self.assertRaises(OSError):
            GetResult('bla1', 'bla2').process()
