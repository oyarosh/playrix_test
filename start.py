#!/usr/bin/python3
from __future__ import absolute_import

import os
import sys
import csv
import argparse
import logging
import unittest

from core.utils import GetResult
from core.tests import GetResultTest


def main():
    parser = argparse.ArgumentParser(description='''
        The program should analyze input files and return csv with result.''')
    base_dir = os.path.dirname(os.path.realpath(__file__)) + '/static'
    parser.add_argument('--base_dir', dest='base_dir',
                        default=base_dir,
                        help='''
                            Base directory where file is located.
                            Default: %s''' % base_dir)
    parser.add_argument('--name_installs', dest='name_installs',
                        default='installs.csv',
                        help='''
                            File name with installs info. Default: installs.csv''')
    parser.add_argument('--name_purchases', dest='name_purchases',
                        default='purchases.csv',
                        help='''
                            File name with purchases info. Default: purchases.csv''')
    args = parser.parse_args()
    installs_path = args.base_dir + '/' + args.name_installs
    purchases_path = args.base_dir + '/' + args.name_purchases

    test_run = input("Run test Y/n (default=n)")
    if test_run and test_run == 'Y':
        suite = unittest.TestLoader().loadTestsFromTestCase(GetResultTest)
        unittest.TextTestRunner(verbosity=2).run(suite)

    if not all([os.path.isfile(installs_path), os.path.isfile(purchases_path)]):
        sys.stdout.write(
            'File does not exist! installs_path {0} purchases_path {1} \r\n'.format(installs_path, purchases_path))
        return

    sys.stdout.write('Start processing')
    result = GetResult(installs_path, purchases_path).process()

    with open(args.base_dir + '/' + 'result_by_yarosh.csv', 'w') as f:
        dw = csv.DictWriter(f, delimiter=',', fieldnames=result[0])
        dw.writeheader()
        dw.writerows(result)

    print ('Result file - {}'.format(args.base_dir + '/' + 'result_by_yarosh.csv'))


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()
